import adapter from '@sveltejs/adapter-static';
export default {
	kit: {
		alias: {
			$vendor: './src/vendor/*'
		},
		adapter: adapter({
			// default options are shown. On some platforms
			// these options are set automatically — see below
			pages: 'build',
			assets: 'build',
			fallback: null,
			precompress: true,
			strict: true
		}),
		paths: {
			base: '/git-pdm-mockups'
		}
	}
};
