
# Git PDM Mockups

These are interactive UI mockups created using [Svelte-Kit](https://kit.svelte.dev/) and [Wired Elements](https://wiredjs.com/).


## Developing

```bash
yarn install
yarn dev

# or start the server and open the app in a new browser tab
yarn dev --open
```

## Building

To create a production version of your app:

```bash
yarn build
```

You can preview the production build with `yarn preview`.

> To deploy your app, you may need to install an [adapter](https://kit.svelte.dev/docs/adapters) for your target environment.
