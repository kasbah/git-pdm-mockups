import { writable } from 'svelte/store';

export const AppNames = [
	'Git PDM',
	'ASM PDM',
	'Scatter PDM',
	'OTFN PDM',
	'Tree PDM',
	'Three Git'
] as const;
export const appName = writable('');
