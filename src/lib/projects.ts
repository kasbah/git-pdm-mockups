import slugify from 'slugify';

const titles = [
	'NC11117 LDO Regulator',
	'GD32E103CBT6 MCU',
	'NCV68261 RP-Protector RP Switch',
	'TPS62135 PoL DC-DC Conv.',
	'HT7533 LDO Precision Regulator',
	'ADP5061 Multi-Chem Charger',
	'LP2985 Ultra LDO Regulator',
	'LT1931 High Eff. DC-DC Conv.',
	'LM2674 Step-Up DC-DC Conv.',
	'MC34063 Step-Up/Step-Down DC-DC Conv.',
	'MC33269 High Eff. DC-DC Conv.',
	'NCV7608 Low-Side Switch',
	'NCP5623 PWM Control',
	'NCP1117 LDO Regulator',
	'NCP1203 PWM Control',
	'NCP1529 LDO Regulator',
	'NCP1605 PWM Control',
	'NCP1851 High Eff. DC-DC Conv.',
	'NCP1852 High Eff. DC-DC Conv.',
	'NCP2820 PWM Control'
] as const;

export interface Project {
	title: string;
	slug: string;
}

export const projects: Readonly<Array<Project>> = titles.map((title) => ({
	title,
	slug: slugify(title)
}));
